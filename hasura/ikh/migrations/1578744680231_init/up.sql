CREATE SCHEMA stocktake;
CREATE TABLE stocktake.scans (
    id integer NOT NULL,
    user_id integer NOT NULL,
    ean text NOT NULL,
    sid integer NOT NULL,
    device_id integer NOT NULL,
    udid text NOT NULL,
    scan_time timestamp with time zone NOT NULL,
    barcode_format text NOT NULL,
    amount integer DEFAULT 1 NOT NULL,
    warehouse_id integer NOT NULL,
    capture_type text DEFAULT 'default_capture type'::text NOT NULL
);
COMMENT ON TABLE stocktake.scans IS 'Data from codereadr scan';
CREATE VIEW stocktake.last_scan_id AS
 SELECT scans.device_id,
    max(scans.id) AS last_id
   FROM stocktake.scans
  GROUP BY scans.device_id;
CREATE SEQUENCE stocktake.scans_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
ALTER SEQUENCE stocktake.scans_id_seq OWNED BY stocktake.scans.id;
ALTER TABLE ONLY stocktake.scans ALTER COLUMN id SET DEFAULT nextval('stocktake.scans_id_seq'::regclass);
ALTER TABLE ONLY stocktake.scans
    ADD CONSTRAINT stocktake_pkey PRIMARY KEY (id);
